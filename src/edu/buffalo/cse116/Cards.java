package edu.buffalo.cse116;
import java.util.ArrayList;
import java.util.Collections;

public class Cards {
	
	ArrayList<String> _suspects = new ArrayList<String>();
	ArrayList<String> _weapons = new ArrayList<String>();
	ArrayList<String> _rooms = new ArrayList<String>();
	ArrayList<String> _deck = new ArrayList<String>();
	ArrayList<String> sepDeck = new ArrayList<String>();
	
	public Cards(){
		_suspects.add("Mr. Green");
		_suspects.add("Mrs. Peacock");
		_suspects.add("Mrs. White");
		_suspects.add("Miss Scarlet");
		_suspects.add("Prof. Plum");
		_suspects.add("Col. Mustard");
		_weapons.add("Knife");
		_weapons.add("Revolver");
		_weapons.add("Candlestick");
		_weapons.add("Rope");
		_weapons.add("Lead Pipe");
		_weapons.add("Wrench");
		_rooms.add("Library");
		_rooms.add("Conservatory");
		_rooms.add("Billiards Room");
		_rooms.add("Kitchen");
		_rooms.add("Lounge");
		_rooms.add("Ballroom");
		_rooms.add("Dining Room");
		_rooms.add("Study");
		_rooms.add("Hall");
	}
	

	public ArrayList<String> separate(){
		Collections.shuffle(_suspects);
		sepDeck.add(_suspects.get(0));
		_suspects.remove(0);
		Collections.shuffle(_rooms);
		sepDeck.add(_rooms.get(0));
		_rooms.remove(0);
		Collections.shuffle(_weapons);
		sepDeck.add(_weapons.get(0));
		_weapons.remove(0);
		return sepDeck;
	}
	
	public ArrayList<String> combine() {
		_deck.addAll(_suspects);
		_deck.addAll(_weapons);
		_deck.addAll(_rooms);
		Collections.shuffle(_deck);
		return _deck;
	}
	
	public String draw(){
		String card = _deck.get(0);
		_deck.remove(_deck.get(0));
		return card;
	}//
	
}
