 package edu.buffalo.cse116;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JSplitPane;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.OverlayLayout;
import javax.swing.JTable;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;


public class GUI extends JFrame {

	private JPanel contentPane;
	GameBoard GB = new GameBoard();
	Mechanics m = new Mechanics();
	
	//protected JButton[][] b = new JButton[25][24];
	protected JButton[][] b = new JButton[GB.row][GB.col];
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
					GUI fr = new GUI();
					fr.setVisible(true);
					fr.setTitle("CLUELESS");
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 1500, 740);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane sP1 = new JSplitPane();
		sP1.setBackground(Color.BLUE);
		contentPane.add(sP1, BorderLayout.CENTER);
		sP1.setPreferredSize(new Dimension(1500, 695));
		
		ImageIcon clue = new ImageIcon(getClass().getResource("clue-board.png"));
		Image img = clue.getImage();
		Image newimg12 = img.getScaledInstance(818, 680, Image.SCALE_SMOOTH);
		clue = new ImageIcon(newimg12);
		
		JPanel picpanel = new JPanel();
		JPanel overlay = new JPanel(){
			
			@Override
			public boolean isOptimizedDrawingEnabled(){
				return false;
				
			}
		};
		
		overlay.setLayout(new OverlayLayout(overlay));
		JLabel label1 = new JLabel("", clue, JLabel.CENTER);
		picpanel.add(label1);
		
		
		
		
		
		JPanel bp = new JPanel();
		overlay.add(bp, BorderLayout.CENTER);
		overlay.add(picpanel, BorderLayout.CENTER);
		
		sP1.setLeftComponent(overlay);
		//bp.setOpaque(false);
		bp.setPreferredSize(new Dimension(750, 525));
		//sP1.setLeftComponent(bp);
		bp.setLayout(new GridLayout(GB.row, GB.col));
		
		
		JSplitPane sP2 = new JSplitPane();
		sP2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		sP1.setRightComponent(sP2);
		
		JPanel sidep = new JPanel();
		JPanel top = new JPanel();
		JPanel bot = new JPanel();
		
		ImageIcon side1 = new ImageIcon(getClass().getResource("panel.png"));
		Image im1 = side1.getImage();
		Image newim1 = im1.getScaledInstance(600, 620, Image.SCALE_SMOOTH);
		side1 = new ImageIcon(newim1);
		
		JLabel spl = new JLabel("", side1, JLabel.CENTER);
		sidep.add(spl);
		
		top.setLayout(new OverlayLayout(top));
		
		
		JPanel cp = new JPanel();
		cp.setPreferredSize(new Dimension(10, 625));
		//cp.setBackground(Color.yellow);
		cp.setOpaque(false);
		top.add(cp, BorderLayout.CENTER);
		top.add(sidep, BorderLayout.CENTER);
		sP2.setTopComponent(top);
		
		
		
		JPanel sidep2 = new JPanel();
		
		ImageIcon side2 = new ImageIcon(getClass().getResource("panel.png"));
		Image im2 = side2.getImage();
		Image newim2 = im2.getScaledInstance(600, 100, Image.SCALE_SMOOTH);
		side2 = new ImageIcon(newim2);
		
		JLabel spl2 = new JLabel("", side2, JLabel.CENTER);
		sidep2.add(spl2);
		
		JPanel np = new JPanel(new BorderLayout());
		//np.setBackground(Color.orange);
		np.setOpaque(false);
		bot.setLayout(new OverlayLayout(bot));
		bot.add(np, BorderLayout.CENTER);
		bot.add(sidep2, BorderLayout.CENTER);
		sP2.setBottomComponent(bot);
		
		
		
		
		//ScoreCard Panel
		TitledBorder border = new TitledBorder("ScoreCard");
	    border.setTitleJustification(TitledBorder.CENTER);
	    border.setTitlePosition(TitledBorder.TOP);
	    np.setBorder(border);
		
		JMenuBar bar = new JMenuBar();
		np.add(bar, BorderLayout.NORTH);
		
		JMenu players = new JMenu("Suspects");
		bar.add(players);
		
		JCheckBoxMenuItem green = new JCheckBoxMenuItem("Mr. Green");
		players.add(green);
		
		JSeparator separator = new JSeparator();
		players.add(separator);
		JCheckBoxMenuItem yellow = new JCheckBoxMenuItem("Col. Mustard");
		players.add(yellow);
		
		JSeparator separator_1 = new JSeparator();
		players.add(separator_1);
		JCheckBoxMenuItem blue = new JCheckBoxMenuItem("Mrs. Peacock");
		players.add(blue);
		
		JSeparator separator_2 = new JSeparator();
		players.add(separator_2);
		JCheckBoxMenuItem plum = new JCheckBoxMenuItem("Prof. Plum");
		players.add(plum);
		
		JSeparator separator_3 = new JSeparator();
		players.add(separator_3);
		JCheckBoxMenuItem red = new JCheckBoxMenuItem("Miss Scarlet");
		players.add(red);
		
		JSeparator separator_4 = new JSeparator();
		players.add(separator_4);
		JCheckBoxMenuItem white = new JCheckBoxMenuItem("Mrs.White");
		players.add(white);

		
		JMenu weapon = new JMenu("Weapons");
		bar.add(weapon);
		
		JCheckBoxMenuItem wr = new JCheckBoxMenuItem("Wrench");
		weapon.add(wr);
		
		JSeparator separator_5 = new JSeparator();
		weapon.add(separator_5);
		JCheckBoxMenuItem cs = new JCheckBoxMenuItem("Candlestick");
		weapon.add(cs);
		
		JSeparator separator_6 = new JSeparator();
		weapon.add(separator_6);
		JCheckBoxMenuItem kn = new JCheckBoxMenuItem("Knife");
		weapon.add(kn);
		
		JSeparator separator_7 = new JSeparator();
		weapon.add(separator_7);
		JCheckBoxMenuItem pst = new JCheckBoxMenuItem("Revolver");
		weapon.add(pst);
		
		JSeparator separator_8 = new JSeparator();
		weapon.add(separator_8);
		JCheckBoxMenuItem lp = new JCheckBoxMenuItem("Lead Pipe");
		weapon.add(lp);
		
		JSeparator separator_9 = new JSeparator();
		weapon.add(separator_9);
		JCheckBoxMenuItem rope = new JCheckBoxMenuItem("Rope");
		weapon.add(rope);
		
		
		JMenu room = new JMenu("Rooms");
		bar.add(room);
		
		JCheckBoxMenuItem br = new JCheckBoxMenuItem("Ballroom");
		room.add(br);
		
		JSeparator separator_10 = new JSeparator();
		room.add(separator_10);
		JCheckBoxMenuItem kitchen = new JCheckBoxMenuItem("Kitchen");
		room.add(kitchen);
		
		JSeparator separator_11 = new JSeparator();
		room.add(separator_11);
		JCheckBoxMenuItem dr = new JCheckBoxMenuItem("Dining Room");
		room.add(dr);
		
		JSeparator separator_12 = new JSeparator();
		room.add(separator_12);
		JCheckBoxMenuItem lounge = new JCheckBoxMenuItem("Lounge");
		room.add(lounge);
		
		JSeparator separator_13 = new JSeparator();
		room.add(separator_13);
		JCheckBoxMenuItem hall = new JCheckBoxMenuItem("Hall");
		room.add(hall);
		
		JSeparator separator_14 = new JSeparator();
		room.add(separator_14);
		JCheckBoxMenuItem st = new JCheckBoxMenuItem("Study");
		room.add(st);
		
		JSeparator separator_15 = new JSeparator();
		room.add(separator_15);
		JCheckBoxMenuItem library = new JCheckBoxMenuItem("Library");
		room.add(library);
		
		JSeparator separator_16 = new JSeparator();
		room.add(separator_16);
		JCheckBoxMenuItem bil = new JCheckBoxMenuItem("Billiard Room");
		room.add(bil);
		
		JSeparator separator_17 = new JSeparator();
		room.add(separator_17);
		JCheckBoxMenuItem cons = new JCheckBoxMenuItem("Conservatory");
		room.add(cons);
		
		
		
		//Card/Button Panel
		ImageIcon dice = new ImageIcon(getClass().getResource("dice-icon.png"));
		Image image = dice.getImage();
		Image newimg = image.getScaledInstance(25, 25, Image.SCALE_SMOOTH);
		dice = new ImageIcon(newimg);
				
		ImageIcon turn = new ImageIcon(getClass().getResource("end-turn.png"));
		Image image1 = turn.getImage();
		Image newimg1 = image1.getScaledInstance(25, 25, Image.SCALE_SMOOTH);
		turn = new ImageIcon(newimg1);
				
		ImageIcon next = new ImageIcon(getClass().getResource("Next-icon.png"));
		Image image2 = next.getImage();
		Image newimg2 = image2.getScaledInstance(25, 25, Image.SCALE_SMOOTH);
		next = new ImageIcon(newimg2);
		cp.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JToolBar toolBar = new JToolBar();
		cp.add(toolBar);
		
		
		JLabel label = new JLabel("");
		label.setIcon(dice);
		toolBar.add(label);
		
		JButton btnEndTurn = new JButton("End Turn", turn);
		toolBar.add(btnEndTurn);
		
		JLabel urup = new JLabel("");
		toolBar.add(urup);
		
		JButton btnGetNextPlayer = new JButton("Next Player", next);
		toolBar.add(btnGetNextPlayer);
		
		JLabel getplayer = new JLabel("");
		toolBar.add(getplayer);
		btnGetNextPlayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m.getPlayer();
				getplayer.setText(String.valueOf(m.getPlayer()));
				getplayer.setVisible(true);
			}
		});
		
		btnEndTurn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m.endTurn();
				urup.setText(String.valueOf(m.URUP()));
				urup.setVisible(true);
				label.setText("Dice Roll: " + String.valueOf(m.diceRoll()));
		    	label.setVisible(true);
			}
		});
		if (m.diceRoll() != 0) {
			label.setText(String.valueOf("Dice Roll: " + m.diceRoll()));
	    	label.setVisible(true);
		}
		
		m.setPlayerNames();
		m.addPlayersToList();
		
		
		//Board Panel
		for(int i = 0; i < GB.row; i++){
			for(int j = 0; j < GB.col; j++){
				b[i][j] = new JButton();
				GB.gb[i][j] = b[i][j];
				b[i][j].setOpaque(false);
				b[i][j].setContentAreaFilled(false);
				b[i][j].setBorderPainted(true);
				bp.add(b[i][j]);
				
				
			}
		}
		
		m.addFreeTiles();
		m.addRoomTiles();
		m.addVoidTiles();
		m.addDoorTiles();
		m.addPlayerTiles();
		m.addPlayersToList();
		m.secretPassagewayTiles();
		
		//Player Pieces
		ImageIcon r = new ImageIcon(getClass().getResource("red.png"));
		Image r1 = r.getImage();
		Image r2 = r1.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		r = new ImageIcon(r2);
		//JButton Red = new JButton(r);	
		
		
		ImageIcon g = new ImageIcon(getClass().getResource("Next-icon.png"));
		Image g1 = g.getImage();
		Image g2 = g1.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		g = new ImageIcon(g2);
		JButton Green = new JButton(g);		
		
		
		ImageIcon p = new ImageIcon(getClass().getResource("Next-icon.png"));
		Image p1 = p.getImage();
		Image p2 = p1.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		p = new ImageIcon(p2);
		JButton Purple = new JButton(p);
		
		
		ImageIcon bl = new ImageIcon(getClass().getResource("Next-icon.png"));
		Image bl1 = bl.getImage();
		Image bl2 = bl1.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		bl = new ImageIcon(bl2);
		JButton Blue = new JButton(bl);	
		
		
		ImageIcon y = new ImageIcon(getClass().getResource("Next-icon.png"));
		Image y1 = y.getImage();
		Image y2 = y1.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		y = new ImageIcon(y2);
		JButton Yellow = new JButton(y);
		
		
		ImageIcon w = new ImageIcon(getClass().getResource("Next-icon.png"));
		Image w1 = w.getImage();
		Image w2 = w1.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
		w = new ImageIcon(w2);
		JButton White = new JButton(w);
		
		
		
		
		for(int i = 0; i < GB.row; i++) {
			for(int j = 0; j < GB.col; j++) {
				if (m.GB.gb[i][j] == "R") {
					
					b[i][j].setBorderPainted(false);
				}
				if (m.GB.gb[i][j] == "V") {
					
					b[i][j].setBorderPainted(false);
				}
				if (m.GB.gb[i][j] == "P") {
					
					b[i][j].setBorderPainted(true);
				}
				if (m.GB.gb[i][j] == "r") {
					
					//b[i][j].setText("1");
					b[i][j].setBackground(Color.red);
					b[i][j].setOpaque(true);
					
				}
				if (m.GB.gb[i][j] == "g") {
					
					//b[i][j].setText("1");
					b[i][j].setBackground(Color.green);
					b[i][j].setOpaque(true);
					
				}
				if (m.GB.gb[i][j] == "b") {
					
					//b[i][j].setText("1");
					b[i][j].setBackground(Color.blue);
					b[i][j].setOpaque(true);
					
				}
				if (m.GB.gb[i][j] == "p") {
					
					//b[i][j].setText("1");
					b[i][j].setBackground(Color.magenta);
					b[i][j].setOpaque(true);
					
				}
				if (m.GB.gb[i][j] == "y") {
					
					//b[i][j].setText("1");
					b[i][j].setBackground(Color.yellow);
					b[i][j].setOpaque(true);
					
				}
				if (m.GB.gb[i][j] == "w") {
					
					//b[i][j].setText("1");
					b[i][j].setBackground(Color.white);
					b[i][j].setOpaque(true);
					
				}
				
				
			}
		}
		bp.setOpaque(false);
	}
}
