package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Random;

public class Mechanics {
	GameBoard GB = new GameBoard();
	tileObject tile = new tileObject(); // to String method
	Object R = new Object(); // creates a new Room Object
	Object F = new Object(); // creates a new Free-space Object
	Object V = new Object(); // creates a new void object
	Object D = new Object(); // creates a new Door object
	/**creates player pieces*/
	Object red = new Object();
	Object purple = new Object();
	Object blue = new Object();
	Object green = new Object();
	Object white = new Object();
	Object yellow = new Object();
	
	Object P = new Object();
	
	public void objectsToString() { // gives the specified object a string representation
		R = tile.toString("R");
		F = tile.toString("F");
		V = tile.toString("V");
		D = tile.toString("D");
		P = tile.toString("P");
		
	}
	
	/**Sets tiles on the game board
	 * 
	 * @param W starting tile in the row
	 * @param X ending tile in the row
	 * @param Y starting tile in the column
	 * @param Z ending tile in the column
	 * @param S letter used to represent tile*/
	public void setTiles(int W, int X, int Y, int Z, Object O, String S) {
		O = tile.toString(S);
		for (int i = W; i < X; i++) {
			for (int j = Y; j < Z; j++) {
				GB.gb[i][j] = O;
			}
		}
	}
	
	/**Adds the free-space tiles to the game board*/
	public void addFreeTiles() {
		setTiles(0, GB.row, 0, GB.col, F, "F");
	}
	
	/**Adds the room-space tiles to the game board*/
	public void addRoomTiles() {
		//Study
		setTiles(0, 5, 0, 1, R, "R");
		setTiles(0, 4, 1, 7, R, "R");
		//Library
		setTiles(6, 7, 0, 6, R, "R");
		setTiles(7, 10, 0, 7, R, "R");
		setTiles(10, 11, 0, 6, R, "R");
		//Billiard Room
		setTiles(11, 18, 0, 1, R, "R");
		setTiles(12, 17, 1, 6, R, "R");
		//Conservatory
		setTiles(19, 20, 0, 5, R, "R");
		setTiles(20, 23, 0, 6, R, "R");
		setTiles(23, 24, 0, 7, R, "R");
		setTiles(24, 25, 0, 9, R, "R");
		//Hall
		setTiles(0, 1, 8, 16, R, "R");
		setTiles(1, 7, 9, 15, R, "R");
		//Stairs
		setTiles(8, 15, 9, 14, R, "R");
		//Ballroom
		setTiles(17, 23, 8, 16, R, "R");
		setTiles(23, 25, 10, 14, R, "R");
		//Lounge
		setTiles(0, 6, 17, 23, R, "R");
		setTiles(0, 7, 23, 24, R, "R");
		//Dining Room
		setTiles(9, 15, 16, 23, R, "R");
		setTiles(8, 17, 23, 24, R, "R");
		setTiles(15, 16, 19, 24, R, "R");
		//Kitchen
		setTiles(18, 23, 18, 24, R, "R");
		setTiles(23, 24, 17, 24, R, "R");
		setTiles(24, 25, 15, 24, R, "R");
	}
	
	public void addVoidTiles() {
		setTiles(8, 15, 9, 14, V, "V");
		setTiles(24, 25, 0, 9, V, "V");
		setTiles(24, 25, 15, 24, V, "V");
		setTiles(0, 1, 6, 7, V, "V");
		setTiles(0, 1, 8, 9, V, "V");
		setTiles(0, 1, 15, 16, V, "V");
		setTiles(4, 5, 0, 1, V, "V");
		setTiles(6, 7, 0, 1, V, "V");
		setTiles(10, 12, 0, 1, V, "V");
		setTiles(17, 18, 0, 1, V, "V");
		setTiles(19, 20, 0, 1, V, "V");
		setTiles(23, 24, 6, 7, V, "V");
		setTiles(23, 24, 17, 18, V, "V");
		setTiles(18, 19, 23, 24, V, "V");
		setTiles(16, 17, 23, 24, V, "V");
		setTiles(8, 9, 23, 24, V, "V");
		setTiles(6, 7, 23, 24, V, "V");
	}
	
	public void addDoorTiles() {
		setTiles(4, 5, 6, 7, D, "D");
		setTiles(8, 9, 7, 8, D, "D");
		setTiles(11, 12, 3, 4, D, "D");
		setTiles(11, 12, 1, 2, D, "D");
		setTiles(15, 16, 6, 7, D, "D");
		setTiles(19, 20, 5, 6, D, "D");
		setTiles(4, 5, 8, 9, D, "D");
		setTiles(7, 8, 11, 13, D, "D");
		setTiles(19, 20, 7, 8, D, "D");
		setTiles(16, 17, 9, 10, D, "D");
		setTiles(16, 17, 14, 15, D, "D");
		setTiles(19, 20, 16, 17, D, "D");
		setTiles(6, 7, 17, 18, D, "D");
		setTiles(8, 9, 17, 18, D, "D");
		setTiles(12, 13, 15, 16, D, "D");
		setTiles(17, 18, 19, 20, D, "D");
	}
	
	public void addPlayerTiles() {
		setTiles(0, 1, 16, 17, red, "r");
		setTiles(5, 6, 0, 1, purple, "p");
		setTiles(18, 19, 0, 1, blue, "b");
		setTiles(24, 25, 9, 10, green, "g");
		setTiles(24, 25, 14, 15, white, "w");
		setTiles(7, 8, 23, 24, yellow, "y");
	}
	
	public void secretPassagewayTiles(){
		setTiles(3, 4, 0, 1, P, "P");
		setTiles(19, 20, 1, 2, P, "P");
		setTiles(23, 24, 18, 19, P, "P");
		setTiles(5, 6, 23, 24, P, "P");
	}
	
	/**Displays the game board to the console*/
	public void displayTiles() {
		for (int i = 0; i < GB.row; i++) {
			for (int j = 0; j < GB.col; j++) {
				System.out.print(GB.gb[i][j]);
				if (j == GB.col - 1) {
					System.out.println();
				}
			}
		}
		System.out.println();
	}

	/**creates weapon pieces*/
	Object rope = new Object();
	Object leadPipe = new Object();
	Object knife = new Object();
	Object wrench = new Object();
	Object candlestick = new Object();
	Object pistol = new Object();
	
	/**creates the playerArray to be filled*/
	Object[] playerArray = new Object[6];
	
	/**@param index index of the playerArray
	 * @param Player the player object to add
	 * @param s allows the method to call Player().getname()*/
	public void addPlayerToArray(int index, Object Player, String s) {
		Player = tile.toString(s);
		playerArray[index] = Player;
	} //this method is adding player pieces to the array. We don't want this.
	
	/**Sets the names of the players*/
	/**populates the playerArray*/
	
	//KEYBOARD MOVEMENT
	 public boolean up(int x, int y, Object o, String s) {
		o = tile.toString(s);
		if(GB.gb[x - 1][y] == o) {
			return true;
		}
		return false;
	}

	public boolean down(int x, int y, Object o, String s) {
		o = tile.toString(s);
		if(GB.gb[x + 1][y] == o) {
			return true;
		}
		return false;
	}

	public boolean left(int x, int y, Object o, String s) {
		o = tile.toString(s);
		if(GB.gb[x][y - 1] == o) {
			return true;
		}
		return false;
	}

	public boolean right(int x, int y, Object o, String s) {
		o = tile.toString(s);
		if(GB.gb[x][y + 1] == o) {
			return true;
		}
		return false;
	}
	
//	public void moveDown() {
//		if(down(GB.row, GB.col, F, "F")) {
//			String P = playerList.get(urUp - 1);
//		}
//	}
	
	private int dr;
	public int diceRoll() {
		Random rn = new Random();
		dr = rn.nextInt(13 - 2) + 2;
		return dr;
	}
	
	Cards C = new Cards();
	public void shuffleCards() {
		C.separate();
		C.combine();
	}
	
	Player p1 = new Player();
	Player p2 = new Player();
	Player p3 = new Player();
	Player p4 = new Player();
	Player p5 = new Player();
	Player p6 = new Player();
	
	public void setPlayerNames() {
		p1.setname("Red");
		p2.setname("Purple");
		p3.setname("Blue");
		p4.setname("Green");
		p5.setname("White");
		p6.setname("Yellow");
	}
	
	ArrayList<String> playerList = new ArrayList<String>();
	
	public void addPlayersToList() {
		playerList.add(p1.getname());
		playerList.add(p2.getname());
		playerList.add(p3.getname());
		playerList.add(p4.getname());
		playerList.add(p5.getname());
		playerList.add(p6.getname());
	}
	
	protected int urUp = 0;
	
	public void endTurn() {
		urUp++;
		if (urUp == 7) {
			urUp = 1;
		}
	}
	
	public Object getPlayer() {
		return playerList.get(urUp - 1);
	}
	public int URUP(){
		return urUp;
	}
}
