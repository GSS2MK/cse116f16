package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class Tests {
	Mechanics m = new Mechanics();
	tileObject tile = new tileObject();
	
	@Test
	public void test_addFreeTiles() {
		m.addFreeTiles();
		assertEquals("F", m.GB.gb[0][0]);
	}
	
	@Test
	public void test_addRoomTiles() {
		m.addFreeTiles();
		m.addRoomTiles();
		assertEquals("R", m.GB.gb[0][6]);
	}
	
	@Test
	public void test_shuffleCards() {
		assertEquals(null, m.C.separate());
	}
	
	@Test
	public void test_shuffleCards2() {
		m.C.separate();
		assertEquals(null, m.C.combine());
	}
	
	@Test
	public void test_addPlayersToList() {
		m.setPlayerNames();
		m.addPlayersToList();
		assertEquals(null, m.playerList);
	}
	
	@Test
	public void test_getPlayer() {
		m.setPlayerNames();
		m.addPlayersToList();
		m.endTurn();
		m.endTurn();
		m.endTurn();
		m.endTurn();
		m.endTurn();
		m.endTurn();
		assertEquals(null, m.getPlayer());
	}
	

	/*KEYBOARD MOVEMENT
	 * @Test
	public void test_moveUp() {
		m.addFreeTiles();
		m.addRoomTiles();
		assertEquals(true, m.moveUp(1, 7, m.F, "F"));
	}
	
	@Test
	public void test_moveDown() {
		m.addFreeTiles();
		m.addRoomTiles();
		assertEquals(true, m.moveDown(0, 7, m.F, "F"));
	}
	
	@Test
	public void test_moveLeft() {
		m.addFreeTiles();
		m.addRoomTiles();
		assertEquals(true, m.moveLeft(5, 1, m.F, "F"));
	}
	
	@Test
	public void test_moveRight() {
		m.addFreeTiles();
		m.addRoomTiles();
		assertEquals(true, m.moveRight(4, 0, m.F, "F"));
	}*/
}
